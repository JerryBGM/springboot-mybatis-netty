package com.view.bp;

import com.view.bp.nettyService.NettyServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetSocketAddress;

@SpringBootApplication
@MapperScan("com.view.bp.mapper")
public class BpA0303Application {
	public static void main(String[] args) {
		SpringApplication.run(BpA0303Application.class, args);
		System.out.println("---------------启动成功------------------");
	}
}
