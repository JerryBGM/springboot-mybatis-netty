package com.view.bp;

import com.view.bp.nettyService.NettyServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;

/**
 * @author hzj
 * @date 2020/12/31 15:25
 */
@Component
public class InStart implements CommandLineRunner {
    @Autowired
    private NettyServer nettyServer;
    @Override
    public void run(String... args){
        nettyServer.start(new InetSocketAddress("127.0.0.1", 10086));
    }
}
