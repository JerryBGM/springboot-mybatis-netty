package com.view.bp.common;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author hzj
 * @date 2020/12/31 10:18
 * 全局异常捕获
 */
@RestControllerAdvice
@Log4j2
public class ExceptionsHandler {

    /** 运行时异常 */
    @ExceptionHandler(RuntimeException.class)
    public AjaxResult runtimeExceptionHandler(RuntimeException ex) {
        log.error("运行时异常：{}", ex.getMessage(), ex);
        return AjaxResult.error("运行时异常");
    }

    /** 空指针异常 */
    @ExceptionHandler(NullPointerException.class)
    public AjaxResult nullPointerExceptionHandler(NullPointerException ex) {
        log.error("空指针异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("空指针异常");
    }

    /** 类型转换异常 */
    @ExceptionHandler(ClassCastException.class)
    public AjaxResult classCastExceptionHandler(ClassCastException ex) {
        log.error("类型转换异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("类型转换异常");
    }
    /** 文件未找到异常 */
    @ExceptionHandler(FileNotFoundException.class)
    public AjaxResult FileNotFoundException(FileNotFoundException ex) {
        log.error("文件未找到异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("文件未找到异常");
    }
    /** 数字格式异常 */
    @ExceptionHandler(NumberFormatException.class)
    public AjaxResult NumberFormatException(NumberFormatException ex) {
        log.error("数字格式异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("数字格式异常");
    }
    /** 安全异常 */
    @ExceptionHandler(SecurityException.class)
    public AjaxResult SecurityException(SecurityException ex) {
        log.error("安全异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("安全异常");
    }
    /** sql异常 */
    @ExceptionHandler(SQLException.class)
    public AjaxResult SQLException(SQLException ex) {
        log.error("sql异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("sql异常");
    }
    /** 类型不存在异常 */
    @ExceptionHandler(TypeNotPresentException.class)
    public AjaxResult TypeNotPresentException(TypeNotPresentException ex) {
        log.error("类型不存在异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("类型不存在异常");
    }

    /** IO异常 */
    @ExceptionHandler(IOException.class)
    public AjaxResult iOExceptionHandler(IOException ex) {
        log.error("IO异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("IO异常");
    }

    /** 未知方法异常 */
    @ExceptionHandler(NoSuchMethodException.class)
    public AjaxResult noSuchMethodExceptionHandler(NoSuchMethodException ex) {
        log.error("未知方法异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("未知方法异常");
    }

    /** 数组越界异常 */
    @ExceptionHandler(IndexOutOfBoundsException.class)
    public AjaxResult indexOutOfBoundsExceptionHandler(IndexOutOfBoundsException ex) {
        log.error("数组越界异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("数组越界异常");
    }
    /** sql语法错误异常 */
    @ExceptionHandler(BadSqlGrammarException.class)
    public AjaxResult BadSqlGrammarException(BadSqlGrammarException ex) {
        log.error("sql语法错误异常：{} ", ex.getMessage(), ex);
        return AjaxResult.error("sql语法错误异常");
    }

    /** 无法注入bean异常 */
    @ExceptionHandler(NoSuchBeanDefinitionException.class)
    public AjaxResult NoSuchBeanDefinitionException(NoSuchBeanDefinitionException ex) {
        log.error("无法注入bean异常 ：{} ", ex.getMessage(), ex);
        return AjaxResult.error("无法注入bean");
    }

    /** Http消息不可读异常 */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public AjaxResult requestNotReadable(HttpMessageNotReadableException ex) {
        log.error("400错误..requestNotReadable：{} ", ex.getMessage(), ex);
        return AjaxResult.error("Http消息不可读");
    }

    /** 400错误 */
    @ExceptionHandler({TypeMismatchException.class})
    public AjaxResult requestTypeMismatch(TypeMismatchException ex) {
        log.error("400错误..TypeMismatchException：{} ", ex.getMessage(), ex);
        return AjaxResult.error("服务器异常");
    }

    /** 500错误 */
    @ExceptionHandler({ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
    public AjaxResult server500(RuntimeException ex) {
        log.error("500错误：{} ", ex.getMessage(), ex);
        return AjaxResult.error("服务器异常");
    }

    /** 栈溢出 */
    @ExceptionHandler({StackOverflowError.class})
    public AjaxResult requestStackOverflow(StackOverflowError ex) {
        log.error("栈溢出：{} ", ex.getMessage(), ex);
        return AjaxResult.error("栈溢出异常");
    }

    /** 除数不能为0 */
    @ExceptionHandler({ArithmeticException.class})
    public AjaxResult arithmeticException(ArithmeticException ex) {
        log.error("除数不能为0：{} ", ex.getMessage(), ex);
        return AjaxResult.error("除数不能为0异常");
    }

    /** 其他错误 */
    @ExceptionHandler({Exception.class})
    public AjaxResult exception(Exception ex) {
        log.error("其他错误：{} ", ex.getMessage(), ex);
        return AjaxResult.error("网络连接失败，请退出后再试");
    }
}
