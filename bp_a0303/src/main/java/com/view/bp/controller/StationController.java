package com.view.bp.controller;

import com.github.pagehelper.PageHelper;
import com.view.bp.common.AjaxResult;
import com.view.bp.common.DateUtil;
import com.view.bp.pojo.PortReceive;
import com.view.bp.pojo.Station;
import com.view.bp.service.PortReceiveService;
import com.view.bp.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author hzj
 * @date 2020/12/31 11:14
 */
@RestController
@RequestMapping("/station")
public class StationController {
    @Autowired
    private StationService stationService;
    @Autowired
    private PortReceiveService portReceiveService;
    @GetMapping
    public AjaxResult list(Station station){
        PageHelper.startPage(station.getPageNum(),station.getPageSize());
        List<Station> stations = stationService.selectList(station);
        return AjaxResult.success(stations);
    }
    @PostMapping
    public AjaxResult add(@RequestBody Station station){
      return stationService.insertStation(station);
    }

    @PostMapping("/add1")
    public AjaxResult add1(@RequestBody String msg){
        PortReceive portReceive = new PortReceive();
        portReceive.setDetails(msg);
        portReceive.setCreateTime(DateUtil.now());
        System.out.println(LocalDateTime.now());
//        int add = portReceiveService.add(portReceive);
        int add = 0;
        if(0==add){
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }
}
