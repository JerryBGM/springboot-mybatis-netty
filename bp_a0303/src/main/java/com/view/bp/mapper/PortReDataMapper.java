package com.view.bp.mapper;

import com.view.bp.pojo.PortReData;

import java.util.List;

/**
 * @author hzj
 * @date 2021/1/8 16:14
 */
public interface PortReDataMapper {
    public List<PortReData> selectList(PortReData portReData);
    public int insert(PortReData portReData);
}
