package com.view.bp.mapper;

import com.view.bp.pojo.PortReData;
import com.view.bp.pojo.PortReInfo;

import java.util.List;

/**
 * @author hzj
 * @date 2021/1/11 9:17
 */
public interface PortReInfoMapper {
    public List<PortReInfo> selectList(PortReInfo portReInfo);
    public int insert(PortReInfo portReInfo);
}
