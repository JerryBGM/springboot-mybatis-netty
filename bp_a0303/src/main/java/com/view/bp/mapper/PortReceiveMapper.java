package com.view.bp.mapper;

import com.view.bp.pojo.PortReceive;

/**
 * @author hzj
 * @date 2021/1/5 16:50
 */
public interface PortReceiveMapper {
    public int add(PortReceive portReceive);
}
