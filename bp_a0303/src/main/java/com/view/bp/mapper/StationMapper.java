package com.view.bp.mapper;

import com.view.bp.pojo.Station;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author hzj
 * @date 2020/12/31 13:35
 */
public interface StationMapper {
    public List<Station> selectList(Station station);
    public int insertStation(Station station);
}
