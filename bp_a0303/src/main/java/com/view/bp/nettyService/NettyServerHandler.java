package com.view.bp.nettyService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.view.bp.common.DateUtil;
import com.view.bp.pojo.*;
import com.view.bp.service.PortReDataService;
import com.view.bp.service.PortReInfoService;
import com.view.bp.service.PortReceiveService;
import com.view.bp.service.StationService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author hzj
 * @date 2020/12/31 15:21
 */
@Component
@Log4j2
@ChannelHandler.Sharable
public class NettyServerHandler extends ChannelInboundHandlerAdapter {
    @Autowired
    private PortReceiveService portReceiveService;
    @Autowired
    private StationService stationService;
    @Autowired
    private PortReDataService portReDataService;
    @Autowired
    private PortReInfoService portReInfoService;

    /**
     * 客户端连接会触发
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("连接成功.................");
    }

    /**
     * 客户端发消息会触发
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        log.info("服务器收到消息:"+msg.toString());
        ctx.write("ok");
        saveData(msg);
        ctx.flush();
    }

    /**
     * 发生异常触发
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception{
        log.info("netty接收完毕.................");
    }

    //断开连接
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        log.info("断开连接.................");
    }

    @Transactional
    private void saveData(Object msg){
        JSONObject json = JSONObject.parseObject(msg.toString().trim());
        String workStationName = json.get(EnumData.WorkStationName.toString()).toString();
        String WorkStationId = json.get(EnumData.WorkStationId.toString()).toString();
        Object data = json.get(EnumData.Data.toString());
        Object info = json.get(EnumData.Info.toString());

        //保存端口接收到的数据
        PortReceive portReceive = new PortReceive();
        portReceive.setDetails(msg.toString());
        portReceive.setCreateTime(DateUtil.now());
        portReceiveService.add(portReceive);

        /**解析数据 **/
        //工站
        Long id;
        Station station = new Station();
        station.setWorkStationID(WorkStationId);
        station.setWorkStationName(workStationName);
        List<Station> stations = stationService.selectList(station);
        if(stations.isEmpty()){
            stationService.insertStation(station);
            id = station.getId();
        }else {
            id = stations.get(0).getId();
        }
        //Data
        PortReData portReData = JSON.parseObject(data.toString(), PortReData.class);
        portReData.setStationId(id);
        portReDataService.insert(portReData);
        //Info
        PortReInfo portReInfo = JSON.parseObject(info.toString(), PortReInfo.class);
        portReInfo.setStationId(id);
        portReInfoService.insert(portReInfo);
    }
}
