package com.view.bp.nettyService;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hzj
 * @date 2020/12/31 15:22
 */
@Component
public class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Autowired
    private NettyServerHandler nettyServerHandler;

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
//        //解码器
//        socketChannel.pipeline().addLast("decoder", new StringDecoder());//CharsetUtil.UTF_8
//        //添加编码器
//        socketChannel.pipeline().addLast("encoder", new StringEncoder());//CharsetUtil.UTF_8

//        socketChannel.pipeline().addLast("decoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE,1,1,2048,2048));
//        socketChannel.pipeline().addLast("encoder", new LengthFieldPrepender(1, false));

        //编码器
        socketChannel.pipeline().addLast(new MyEncodeHandler());
        //解码器
        socketChannel.pipeline().addLast(new MyDecoderHandler());

        socketChannel.pipeline().addLast(nettyServerHandler);
    }
}
