package com.view.bp.pojo;

import lombok.Data;

/**
 * @author hzj
 * @date 2021/1/5 10:13
 */
@Data
public class BasePojo {
    private int pageNum;
    private int pageSize;
}
