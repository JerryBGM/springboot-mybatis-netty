package com.view.bp.pojo;

import lombok.Data;

/**
 * @author hzj
 * @date 2021/1/5 13:14
 * 字段对应关系
 */
public enum EnumData {
    WorkStationName,//工站名称
    WorkStationId,//工站工号
    CurrentTime,//当前时间
    BootTime,//开机时间
    RunTime,//运行时间
    LastOffTime,//（上次）关机时间
    LastFaultTime,//（上次）故障时间
    WorkStationStatus,//空闲0，工作1，故障2，需要维护3
    BootDuration,//开机时长
    RunDuration,//运行时长
    FaultDuration,//故障时长

    Data,//Data部分
    PlannedDailyOutput,//计划日产能
    CurrentDateCompleteOutput,//当日完成产能
    CurrentDateCompleteRatio,//当日完成百分比
    StandardOutput,//标准产能（当前每小时标准产出量）
    RateEfficiency,//净开动率: 当日完成产能*负荷时间/开机时长
    OperationalEfficiency,//速度开动率：标准加工周期/实际加工周期
    TimeEfficiency,//时间开动率：开机时长/负荷时间（其中负荷时间 = 运行时长-故障时长-设备调整时长）
    PerformanceEfficiency,//性能开动率：净开动率*速度开动率
    QualityRate,//合格品率：合格数量/加工数量
    OEE,//(OverallEquipmentEffectiveness)工站综合效率：时间开动率\*性能开动率\*合格品率

//    WorkStationName,//工站名称
//    WorkStationID,//工站工号
    Info,//Info部分
    WorkshopLeader,//车间负责人
    WorkshopLeaderTEL,//车间负责人联系电话
    WorkStationOperator,//工站操作员
    WorkStationOperatorTEL,//工站操作员联系电话
    WorkStationMaintenanceMan,//工站维修员
    WorkStationMaintenanceManTEL,//工站维修员联系电话
    AddressOfDetailsOfExceptions,//异常查看地址
    AddressOfProductivityAnalysis,//产能分析地址
    AddressOfOperationSpecificationOfWorkStation,//操作手册地址
    AddressOfOperationLogOfWorkStation,//操作日志地址
}
