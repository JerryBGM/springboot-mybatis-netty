package com.view.bp.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author hzj
 * @date 2021/1/6 15:20
 */
@Data
public class PortReData extends BasePojo{
    private Long id;

    private Long stationId;
    //当前时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date currentTime;
    //开机时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date bootTime;
    //运行时间
    private String runTime;
    //（上次）关机时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastOffTime;
    //（上次）故障时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastFaultTime;
    //空闲0，工作1，故障2，需要维护3
    private int workStationStatus;
    //开机时长
    private String bootDuration;
    //故障时长
    private String faultDuration;
    //计划日产能
    private String plannedDailyOutput;
    //当日完成产能
    private String currentDateCompleteOutput;
    //当日完成百分比
    private String currentDateCompleteRatio;
    //标准产能（当前每小时标准产出量）
    private String standardOutput;
    //净开动率: 当日完成产能*负荷时间/开机时长
    private String rateEfficiency;
    //速度开动率：标准加工周期/实际加工周期
    private String operationalEfficiency;
    //时间开动率：开机时长/负荷时间（其中负荷时间 = 运行时长-故障时长-设备调整时长）
    private String timeEfficiency;
    //性能开动率：净开动率*速度开动率
    private String performanceEfficiency;
    //合格品率：合格数量/加工数量
    private String qualityRate;
    //工站综合效率：时间开动率\*性能开动率\*合格品率
    private String oee;
}
