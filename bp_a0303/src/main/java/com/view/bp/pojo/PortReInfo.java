package com.view.bp.pojo;

import lombok.Data;

/**
 * @author hzj
 * @date 2021/1/6 16:13
 */
@Data
public class PortReInfo extends BasePojo {
    private Long id;
    private Long stationId;
    //车间负责人
    private String workshopLeader;
    //车间负责人联系电话
    private String workshopLeaderTel;
    //工站操作员
    private String workStationOperator;
    //工站操作员联系电话
    private String workStationOperatorTEL;
    //工站维修员
    private String workStationMaintenanceMan;
    //工站维修员联系电话
    private String workStationMaintenanceManTEL;
    //异常查看地址
    private String addressOfDetailsOfExceptions;
    //产能分析地址
    private String addressOfProductivityAnalysis;
    //操作手册地址
    private String addressOfOperationSpecificationOfWorkStation;
    //操作日志地址
    private String addressOfOperationLogOfWorkStation;
}
