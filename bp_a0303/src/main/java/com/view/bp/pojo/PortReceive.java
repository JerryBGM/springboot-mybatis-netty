package com.view.bp.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.netty.buffer.ByteBuf;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author hzj
 * @date 2021/1/5 13:55
 */
@Data
public class PortReceive {
    private Long id;
    private String details;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
