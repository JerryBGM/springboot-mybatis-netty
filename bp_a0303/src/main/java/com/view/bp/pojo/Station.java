package com.view.bp.pojo;

import lombok.Data;

/**
 * @author hzj
 * @date 2020/12/31 10:59
 */
@Data
public class Station extends BasePojo{
    private Long id;
    //工站编号
    private String WorkStationID;
    //工站名称
    private String WorkStationName;
}
