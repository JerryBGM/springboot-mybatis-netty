package com.view.bp.service;

import com.view.bp.common.AjaxResult;
import com.view.bp.pojo.PortReData;
import com.view.bp.pojo.PortReInfo;

/**
 * @author hzj
 * @date 2021/1/8 16:12
 */
public interface PortReDataService {
    public AjaxResult insert(PortReData portReData);
}
