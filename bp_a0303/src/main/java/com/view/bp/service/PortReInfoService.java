package com.view.bp.service;

import com.view.bp.common.AjaxResult;
import com.view.bp.pojo.PortReInfo;

import java.util.List;

/**
 * @author hzj
 * @date 2021/1/11 9:17
 */
public interface PortReInfoService {
    public AjaxResult insert(PortReInfo portReInfo);
    public List<PortReInfo> selectList(PortReInfo portReInfo);
}
