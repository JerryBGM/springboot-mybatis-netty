package com.view.bp.service;

import com.view.bp.pojo.PortReceive;
import org.springframework.stereotype.Component;

/**
 * @author hzj
 * @date 2021/1/5 16:48
 */
public interface PortReceiveService {
    public int add(PortReceive portReceive);
}
