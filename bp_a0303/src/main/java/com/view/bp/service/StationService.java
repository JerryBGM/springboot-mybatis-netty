package com.view.bp.service;

import com.view.bp.common.AjaxResult;
import com.view.bp.pojo.Station;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hzj
 * @date 2020/12/31 13:48
 */
public interface StationService {
    public List<Station> selectList(Station station);
    public AjaxResult insertStation(Station station);
}
