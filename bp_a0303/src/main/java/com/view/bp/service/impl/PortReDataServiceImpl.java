package com.view.bp.service.impl;

import com.view.bp.common.AjaxResult;
import com.view.bp.mapper.PortReDataMapper;
import com.view.bp.pojo.PortReData;
import com.view.bp.service.PortReDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hzj
 * @date 2021/1/8 16:12
 */
@Service
public class PortReDataServiceImpl implements PortReDataService {
    @Autowired
    private PortReDataMapper portReDataMapper;
    @Override
    public AjaxResult insert(PortReData portReData) {
        int insert = portReDataMapper.insert(portReData);
        if(0==insert){
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }
}
