package com.view.bp.service.impl;

import com.view.bp.common.AjaxResult;
import com.view.bp.mapper.PortReInfoMapper;
import com.view.bp.pojo.PortReInfo;
import com.view.bp.service.PortReInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hzj
 * @date 2021/1/11 9:18
 */
@Service
public class PortReInfoServiceImpl implements PortReInfoService {
    @Autowired
    private PortReInfoMapper portReInfoMapper;
    @Override
    public AjaxResult insert(PortReInfo portReInfo) {
        int insert = portReInfoMapper.insert(portReInfo);
        if(0==insert){
            return AjaxResult.error();
        }
        return AjaxResult.error();
    }

    @Override
    public List<PortReInfo> selectList(PortReInfo portReInfo) {
        return portReInfoMapper.selectList(portReInfo);
    }
}
