package com.view.bp.service.impl;

import com.view.bp.mapper.PortReceiveMapper;
import com.view.bp.pojo.PortReceive;
import com.view.bp.service.PortReceiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hzj
 * @date 2021/1/5 16:48
 */
@Service
public class PortReceiveServiceImpl implements PortReceiveService {
    @Autowired
    private PortReceiveMapper portReceiveMapper;
    @Override
    public int add(PortReceive portReceive) {
        return portReceiveMapper.add(portReceive);
    }
}
