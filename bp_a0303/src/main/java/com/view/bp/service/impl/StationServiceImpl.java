package com.view.bp.service.impl;

import com.view.bp.common.AjaxResult;
import com.view.bp.mapper.StationMapper;
import com.view.bp.pojo.Station;
import com.view.bp.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hzj
 * @date 2020/12/31 13:51
 */
@Service
public class StationServiceImpl implements StationService {
    @Autowired
    private StationMapper stationMapper;
    @Override
    public List<Station> selectList(Station station) {
        List<Station> stations = stationMapper.selectList(station);
        return stations;
    }

    @Override
    public AjaxResult insertStation(Station station) {
        int i = stationMapper.insertStation(station);
        if(i>0){
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }
}
